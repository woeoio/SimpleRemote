# SimpleRemote
### Remote Administration Tools远程管理工具

### 轻量级、选项卡式、免费、开源的远程连接管理工具支持RDP、SSH、Telnet协议

### 官网：http://www.91fk.net/

### 特征：

- #### 轻量级（无过多的依赖）

- #### 绿色单文件，不到3M(压缩后)

- #### 没有任何广告

- #### 选项卡样式，同时也支持窗口式

- #### 干净，一致的界面（MahApps.Metro）

- #### 内置多套SSH、Telent配色方案

- #### 使用ActiveX、Putty建立连接

- #### 保存的远程连接信息经过加密，支持自定义密码

### 截图：

![contraction](https://gitee.com/uploads/images/2019/0505/212649_cbd1ab3f_1237885.png)

![sshlink](https://gitee.com/uploads/images/2019/0505/212725_35af0864_1237885.png)

![externalwindow](https://gitee.com/uploads/images/2019/0505/212737_97a18e5f_1237885.png)
